const Koa = require('koa'); // ядро
const Router = require('koa-router'); // маршрутизация
const bodyParser = require('koa-bodyparser'); // парсер для POST запросов
const serve = require('koa-static'); // модуль, который отдает статические файлы типа index.html из заданной директории
const logger = require('koa-logger'); // опциональный модуль для логов сетевых запросов. Полезен при разработке.

const app = new Koa();
const router = new Router();
app.use(serve('public'));
app.use(logger());
app.use(bodyParser());
const passport = require('koa-passport'); //реализация passport для Koa
const LocalStrategy = require('passport-local'); //локальная стратегия авторизации
const JwtStrategy = require('passport-jwt').Strategy; // авторизация через JWT
const ExtractJwt = require('passport-jwt').ExtractJwt; // авторизация через JWT



const jwtsecret = "mysecretkey"; // ключ для подписи JWT
const jwt = require('jsonwebtoken'); // аутентификация по JWT для hhtp

passport.use(new LocalStrategy({
            usernameField: 'login',
            passwordField: 'password',
            session: false
        },
        function (login, password, done) {

                if (login=='j16'&&password=='123') {
                    console.log('Вход выполнен.');
                    return done(null, user);
                } else {console.log('Вход не выполнен.')}
                return done(null,false);

            })
    );

// Ждем JWT в Header

const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('jwt'),
    secretOrKey: jwtsecret
};

passport.use(new JwtStrategy(jwtOptions, function (payload, done) {
           if (err) {
                return done(err)
            }
            if (user) {
                done(null, user)
            } else {
                done(null, false)
            }

    })
);

router.post('/login', async(ctx, next) => {
    await passport.authenticate('local', function (err, user) {
        if (user == false) {
            ctx.body = "Login failed";
        } else {
            //--payload - информация которую мы храним в токене и можем из него получать
            const payload = {
                id: '1',
                displayName: 'Julia',
                login: 'j16'
            };
            const token = jwt.sign(payload, jwtsecret); //здесь создается JWT

            ctx.body = {user: 'julia', token: 'JWT ' + token};
        }
    })(ctx, next);
});

router.get('/custom', async(ctx, next) => {

    await passport.authenticate('jwt', function (err, user) {
        if (user) {
            ctx.body = "hello " + user.displayName;
        } else {
            ctx.body = "No such user";
            console.log("err", err)
        }
    } )(ctx, next)
});

app.use(passport.initialize()); // сначала passport
app.use(router.routes()); // потом маршруты
const server = app.listen(3000);// запускаем сервер на порту 3000